<h1>README</h1>
This the the frontend for StonkPix, built with React TypeScript, styled with TailWind CSS.

<h2>To run:</h2>

1. Open the frontend folder in VSCode
2. Delete the `node_modules` folder
3. In the VSCode terminal, run `npm install`
4. In the VSCode terminal, run `npm run dev`
5. You should see the site running at localhost:3000
